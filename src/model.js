/**
 * The generic model validates each property based on type
 * finds any dom elements that are newly inserted and adds them to the model
 * and returns an the domains object model of validated properties
 */

/**
 * [Model description]
 * @param {[type]} options [description]
 */
(function(framework) {

	var model = function(options) {

		function validateModel() {

			if (!validateProperties()) {
				throw new Exception('properties are undefined');
			}

			if (!verifyRequiredOptions()) {
				throw new Exception('required model options are missing');
			}

			if (!verifyRequiredProperties()) {
				throw new Exception('required model properties are missing');
			}
			
			var modelValidation = validateSchema();

			if (modelValidation !== true) {
				modelValidation.forEach(function(error) { console.log(error.error); }.bind(this));
				throw new Exception('model schema for `'+options.properties.name+'` did not validate');
			}
			
		}

		var verifyRequiredOptions = function verifyRequiredOptions() {
			return __.verifyPropertiesExist(options, ['schema', 'properties']);
		};

		function verifyRequiredProperties() {
			return __.verifyPropertiesExist(options.properties, []);
		}

		function validateProperties() {
			return __.validateAllProperties(options);
		}

		function validateSchema() {
			return __validate(options.properties, options.schema);
		}

		// var initFunctions = [validateModel];

		// function init() {
		// 	  initFunctions.forEach(function(fn) { fn.call(this); }.bind(this));
		// }

		__.runFunctions({
			fn: [validateModel],
			scope: this,
			args: [options]
		});
 
		//init();

		return options;

	};

	framework.model = model;

})(window.framework);