/**
 * the domain class is an interface for providing single line
 * instantiation for a domain's model, view and controller.
 * 
 */

var Domain = function(options) {

	if (typeof options !== 'object' ||
		!options.model ||
		!options.view ||
		!options.controller) {
		return false;
	}

	this.options = __.cloneObject(options);
	this.count = 0;
	this.init(options);

};

/**
 * instantiates the view
 * this loads the template and inserts it into the dom
 * @param  {[type]} options [description]
 * @return {[type]}         [description]
 */
Domain.prototype.initView = function(options) {
	var view;

	try {
		view = framework.view.init({
			view: options.view,
			name: options.name,
			number: this.count
		});

	} catch (e) {
		throw new Error(e);
	}

	return view;

};

/**
 * [initModel description]
 * @param  {[type]} options [description]
 * @return {[type]}         [description]
 */
Domain.prototype.initModel = function(options) {
	var model;
	
	try {
		model = framework.model(options.model);
	} catch(e) {
		throw new Error(e);
	}

	return model;
};

/**
 * [initController description]
 * @param  {[type]} options [description]
 * @return {[type]}         [description]
 */
Domain.prototype.initController = function(options) {
	var controller;

	try {
		controller = framework.controller.init({
			model: options.model,
			view: options.view,
			controller: options.controller,
			name: options.name,
			number: this.count,
			mixins: options.mixins
		});

	} catch(e) {
		throw new Error(e);
	}
		
	return controller;
};

/**
 * @todo if no number property, add one
 * @param  {[type]} options [description]
 * @return {[type]}         [description]
 */
Domain.prototype.init = function(options) {
	__.runFunctions({
		fn: [
			this.initView,
			this.initModel,
			this.initController
		],
		args: [options],
		scope: this
	});

};

/**
 * @todo refactor chaining to allow for multiple arguments
 * @param  {[type]} options [description]
 * @return {[type]}         [description]
 */
Domain.prototype.updateOptions = function(options) {
	return options[1] === undefined ? options[0] : __.updateObjectProperties(options[0], options[1]);
};

Domain.prototype.updateOptionLoadOnInit = function(options) {
	return __.updateObjectProperties(options, {
		view: {
			options: {
				loadOnInit: true
			}
		}
	});
};

/**
 * [add description]
 * @param {[type]} options [description]
 */
Domain.prototype.add = function(options) {
	// @todo hack
	this.count = this.count + 1;
	return __.chainMethods({
		functions: [this.updateOptions, this.updateOptionLoadOnInit, this.init],
		args: [__.cloneObject(this.options), options],
		scope: this
	});

};

if (!framework || framework === 'undefined') {
	window.framework = {};
}

framework.Domain = Domain;