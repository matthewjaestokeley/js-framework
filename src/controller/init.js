(function(framework) {

	/**
	 * [init description]
	 * @param  {[type]} options [description]
	 * @return {[type]}         [description]
	 */
	function init(options) {

		__.runFunctions({
			fn: [framework.controller.inherit, framework.controller.validateOptions],
			scope: this,
			args: [options]
		});

		return framework.controller.instantiate(options);
	}

	if (!framework) {
		framework = {};
	}

	if (!framework.controller) {
		framework.controller = {};
	}

	framework.controller.init = init;

})(window.framework);

