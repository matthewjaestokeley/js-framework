(function(framework) {
		/**
		 * [inherit description]
		 * @return {[type]} [description]
		 */
		var inherit = function(options) {
			return options.controller.mixins ? inheritMixins(options.controller.controller, options.controller.mixins) : false;
		};

		/**
		 * [inheritMixins description]
		 * @param  {[type]} controller [description]
		 * @param  {[type]} mixins  [description]
		 * @return {[type]}            [description]
		 */
		var inheritMixins = function(controller, mixins) {
			return validateMixins(mixins) ? mapMixins(mixins, controller) : false;
		};

		/**
		 * [validateMixins description]
		 * @param  {[type]} mixins [description]
		 * @return {[type]}           [description]
		 */
		var validateMixins = function(mixins) {
			return !is.array(mixins) || mixins.length === 0 ? false : true;
		};

		/**
		 * [mapMixins description]
		 * @param  {[type]} mixins  [description]
		 * @param  {[type]} controller [description]
		 * @return {[type]}            [description]
		 */
		var mapMixins = function(mixins, controller) {
		    mixins.map(function(mixin) { inheritMixin(mixin, controller); }.bind(this));
		};

		/**
		 * [inheritMixin description]
		 * @param  {[type]} mixin   [description]
		 * @param  {[type]} controller [description]
		 * @return {[type]}            [description]
		 */
		var inheritMixin = function(mixin, controller) {
			__.inherit(mixin, controller);
		};

	if (!framework) {
		framework = {};
	}

	if (!framework.controller) {
		framework.controller = {};
	}

	framework.controller.inherit = inherit;

})(window.framework);