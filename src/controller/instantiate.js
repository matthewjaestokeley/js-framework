(function(framework) {

	/**
	 * [instantiateController description]
	 * @param  {[type]} options [description]
	 * @return {[type]}         [description]
	 */
	var instantiateController = function instantiateController(options) {
		return new options.controller.controller(options.model, options.view);
	};

	if (!framework) {
		framework = {};
	}

	if (!framework.controller) {
		framework.controller = {};
	}

	framework.controller.instantiate = instantiateController;

})(window.framework);