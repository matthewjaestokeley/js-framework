(function(framework) {
	/**
	 * [validateOptions description]
	 * @return {[type]} [description]
	 */
	var validate = function() {
		
		if(!__.verifyPropertiesExist(options, ['model', 'view', 'controller', 'name', 'number'])) {
		    throw new Exception('controller options are missing a required property');
		}
 		
		if(!validateMethods(options.controller.controller)) {
		    throw new Exception('Controllers must have an `init` and a `listen` method');
		}

		return true;

	};

	/**
	 * [validateMethods description]
	 * @param  {[type]} controller [description]
	 * @return {[type]}            [description]
	 */
	var validateMethods = function(controller) {
		return __.validateMethods(controller, ['init', 'listen']);
	};

	if (!framework) {
		framework = {};
	}

	if (!framework.controller) {
		framework.controller = {};
	}

	framework.controller.validate = validate;

})(window.framework);