/**
 * the generic view fetches the template from server or
 * accepts an html string and handles inserting the dom
 * @todo  bundle multiple template requests
 * 
 */
(function(framework, events) {

 	/**
	 * [View description]
	 * @param {[type]} options [description]
	 */
	var init = function(options) {
		/**
		 * @todo  organize constructor properties
		 * [endpoint description]
		 * @type {[type]}
		 */
		function formatOptions(options) {
			/**
			 * [addCount description]
			 * @todo  refactor
			 * @param {[type]} options [description]
			 * @param {[type]} number  [description]
			 */
			function addCount(options) {
				options[0].properties.number = options[1];
				return options;
			}

			/**
			 * [formatLoadOnInit description]
			 * @param  {[type]} options [description]
			 * @return {[type]}         [description]
			 */
			function formatLoadOnInit(options) {
				options[0].options.loadOnInit = options[0].options.loadOnInit === undefined ? true : options[0].options.loadOnInit;
				return options[0];
			}

			/**
			 * [functions description]
			 * @type {Array}
			 */
			return __.chainMethods({
				functions: [addCount, formatLoadOnInit],
				scope: this,
				args: [options.view, options.number]
			});

		}

		/**
		 * [view description]
		 * @type {[type]}
		 */
		var view = __.runFunctions({
			fn: [framework.view.validate, framework.view.listen, framework.view.sort],
			scope: this,
			args: [formatOptions(options)]
		});

	};

	/**
	 * [if description]
	 * @param  {[type]} !framework [description]
	 * @return {[type]}            [description]
	 */
	if (!framework) {
		framework = {};
	}

	/**
	 * [if description]
	 * @param  {[type]} !framework.view [description]
	 * @return {[type]}                 [description]
	 */
	if (!framework.view) {
		framework.view = {};
	}

	/**
	 * [init description]
	 * @type {[type]}
	 */
	framework.view.init = init;

})(window.framework, window.events);