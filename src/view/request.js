(function(framework, events) {

	/**
	 * [parse description]
	 * @param  {[type]} apiOptions [description]
	 * @return {[type]}            [description]
	 */
	function request(apiOptions, data) {

		/**
		 * [init description]
		 * @param  {[type]} apiOptions [description]
		 * @return {[type]}            [description]
		 */
		function init(apiOptions, data) {
			try {
				
				__.runFunctions({
					fn: [sendRequest],
					scope: this,
					args: [apiOptions, data]
				});

			} catch(e) {
				console.log(e);
				throw new Error(e.message);
			}
		
		}

		/**
		 * [formatRequest description]
		 * @param  {[type]} apiOptions [description]
		 * @return {[type]}            [description]
		 */
		function formatRequest(apiOptions) {

			__.updateObjectProperties(apiOptions, {
				data: {
					name: name
				}
			});
		
		}

		/**
		 * [sendRequest description]
		 * @param  {[type]} apiOptions [description]
		 * @return {[type]}            [description]
		 */
		function sendRequest(apiOptions, data) {
			new Request({
				name: data.name + "-" + data.number + "-template",
	            url: apiOptions.config.protocol + '://' + apiOptions.config.host + ':' + apiOptions.config.port + '/' + apiOptions.config.path,
	            authentication: apiOptions.config.authentication || null,
	            method: apiOptions.config.method || 'POST',
	            data: {
	            	name: apiOptions.template,
	            	data: data
	            },
	            callback: apiOptions.callback || null,
	            headers: apiOptions.config.headers || {}
	        });

		}

		init.call(this, __.cloneObject(apiOptions), __.cloneObject(data));
	
	}


	if (!framework) {
		framework = {};
	}

	if (!framework.view) {
		framework.view = {};
	}

	framework.view.request = request;


})(window.framework, window.events);