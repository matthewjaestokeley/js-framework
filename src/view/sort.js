(function(framework, events) {
	/**
	 * [sort description]
	 * @param  {[type]} options [description]
	 * @return {[type]}         [description]
	 */
	function sort(options) {

		if (options.options.loadOnInit &&
			options.properties.template) {
			framework.view.insert(options, options.properties.template);
		}

		// @todo  validate api object for required properties
		if (options.options.loadOnInit &&
			!options.properties.template &&
			options.options.api) {
			framework.view.request(options.options.api, options.properties);
		}
	}

	if (!framework) {
		framework = {};
	}

	if (!framework.view) {
		framework.view = {};
	}

	framework.view.sort = sort;

})(window.framework, window.events);