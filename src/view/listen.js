(function(framework, events) {

	/**
	 * [listen description]
	 * @param  {[type]} options [description]
	 * @return {[type]}         [description]
	 */
	function listen(options) {
		events.addListener(options.properties.name + "-" + options.properties.number + "-template-transfer-complete", function(data) { framework.view.parseResults.call(this, options, data); }.bind(this));
	}

	if (!framework) {
		framework = {};
	}

	if (!framework.view) {
		framework.view = {};
	}

	framework.view.listen = listen;

})(window.framework, window.events);