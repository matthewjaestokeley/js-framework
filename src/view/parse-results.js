(function(framework, events) {

	/**
	 * [parseResults description]
	 * @param  {[type]} options [description]
	 * @param  {[type]} data    [description]
	 * @return {[type]}         [description]
	 */
	function parseResults(options, data) {
		framework.view.insert(options, JSON.parse(data.target.response).template);
	}

	if (!framework) {
		framework = {};
	}

	if (!framework.view) {
		framework.view = {};
	}

	framework.view.parseResults = parseResults;

})(window.framework, window.events);