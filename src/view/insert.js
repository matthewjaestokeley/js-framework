(function(framework, events) {
	/**
	 * [insert description]
	 * @param  {[type]} options  [description]
	 * @param  {[type]} template [description]
	 * @return {[type]}          [description]
	 */
	function insert(options, template) {
		return __.runFunctions({
			fn: [append, emit],
			scope: this,
			args: [options.properties.name + '-' + options.properties.number, options.properties.parent, template]
		});
	}

	/**
	 * [append description]
	 * @param  {[type]} name     [description]
	 * @param  {[type]} parent   [description]
	 * @param  {[type]} template [description]
	 * @return {[type]}          [description]
	 */
	function append(name, parent, template) {
		parent.appendChild($$.stringToNode(template));
	}

	/**
	 * [emit description]
	 * @param  {[type]} name     [description]
	 * @param  {[type]} parent   [description]
	 * @param  {[type]} template [description]
	 * @return {[type]}          [description]
	 */
	function emit(name, parent, template) {
		events.emit(name + '-view-loaded');
	}

	if (!framework) {
		framework = {};
	}

	if (!framework.view) {
		framework.view = {};
	}

	framework.view.insert = insert;

})(window.framework, window.events);