(function(framework, events) {

	/**
	 * [validate description]
	 * @param  {[type]} options [description]
	 * @return {[type]}         [description]
	 */
	function validate(options) {
		/**
		 * [initializeValidation description]
		 * @param  {[type]} options [description]
		 * @return {[type]}         [description]
		 */
		function initializeValidation(options) {
			if (!validateRequiredOptions(options)) {
				throw new Exception('required options for the view are missing');
			}

			if (!validateApiOptions(options)) {
				throw new Exception('required api options for the view are missing');
			}

			var viewModelValidation = validateViewModel(options);
			
			if (viewModelValidation !== true) {
				  viewModelValidation.forEach(function(error) { console.log(error.error); }.bind(this));
				  throw new Exception('view model schema did not validate');
			}

			return true;

		}


		/**
		 * [validateRequiredOptions description]
		 * @param  {[type]} options [description]
		 * @return {[type]}         [description]
		 */
		function validateRequiredOptions(options) {
			return __.verifyPropertiesExist(options.options, []);
		}

		/**
		 * [validateApiOptions description]
		 * @param  {[type]} options [description]
		 * @return {[type]}         [description]
		 */
		function validateApiOptions(options) {
			return __.verifyPropertiesExist(options.options.api, ['template']);
		}

		/**
		 * [validateViewModel description]
		 * @param  {[type]} options [description]
		 * @return {[type]}         [description]
		 */
		function validateViewModel(options) {
		    return __validate(options.properties, options.schema);
		}

		initializeValidation.call(this, options);

	}

	if (!framework) {
		framework = {};
	}

	if (!framework.view) {
		framework.view = {};
	}

	framework.view.validate = validate;

})(window.framework, window.events);