(function(framework) {

	var ResponseStatusValidation = function() {

	};

	ResponseStatusValidation.prototype.responseStatusValidateReadyState = function(readyState) {
		if (readyState !== 4) {
			return false;
		}
		return true;
	};

	ResponseStatusValidation.prototype.responseStatusValidateStatusCode = function(statusCode) {
		if (statusCode !== 200) {
			return false;
		}
		return true;
	};

	ResponseStatusValidation.prototype.returnValidFormattedResponseStatus = function(response) {
		if (typeof(response) === 'string') {
			if (response[0] === '{' ||
			response[0 === '[']) {
				return JSON.parse(response);
			} else {
				return response;
			}
		}
	};

	ResponseStatusValidation.prototype.validateResponseStatus = function(response) {
		if (!this.responseStatusValidateReadyState(response.readyState) ||
			!this.responseStatusValidateStatusCode(response.status)) {
			return false;
		}
		return this.returnValidFormattedResponseStatus(response.response);

	};

	/**
	 * @param  {[type]} !framework.editor [description]
	 * @return {[type]}             [description]
	 */
	if (!framework.abstracts || framework.abstracts === 'undefined') {
		framework.abstracts = {};
	}

	framework.abstracts.ResponseStatusValidation = ResponseStatusValidation;

})(window.framework);
