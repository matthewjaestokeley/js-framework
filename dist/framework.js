window.framework = {};;(function(framework) {

	/**
	 * [init description]
	 * @param  {[type]} options [description]
	 * @return {[type]}         [description]
	 */
	function init(options) {

		__.runFunctions({
			fn: [framework.controller.inherit, framework.controller.validateOptions],
			scope: this,
			args: [options]
		});

		return framework.controller.instantiate(options);
	}

	if (!framework) {
		framework = {};
	}

	if (!framework.controller) {
		framework.controller = {};
	}

	framework.controller.init = init;

})(window.framework);

;(function(framework) {

	/**
	 * [instantiateController description]
	 * @param  {[type]} options [description]
	 * @return {[type]}         [description]
	 */
	var instantiateController = function instantiateController(options) {
		return new options.controller.controller(options.model, options.view);
	};

	if (!framework) {
		framework = {};
	}

	if (!framework.controller) {
		framework.controller = {};
	}

	framework.controller.instantiate = instantiateController;

})(window.framework);;(function(framework) {

		/**
		 * [inherit description]
		 * @return {[type]} [description]
		 */
		var inherit = function(options) {
			return options.controller.mixins ? inheritMixins(options.controller.controller, options.controller.mixins) : false;
		};

		/**
		 * [inheritMixins description]
		 * @param  {[type]} controller [description]
		 * @param  {[type]} mixins  [description]
		 * @return {[type]}            [description]
		 */
		var inheritMixins = function(controller, mixins) {
			return validateMixins(mixins) ? mapMixins(mixins, controller) : false;
		};

		/**
		 * [validateMixins description]
		 * @param  {[type]} mixins [description]
		 * @return {[type]}           [description]
		 */
		var validateMixins = function(mixins) {
			return !is.array(mixins) || mixins.length === 0 ? false : true;
		};

		/**
		 * [mapMixins description]
		 * @param  {[type]} mixins  [description]
		 * @param  {[type]} controller [description]
		 * @return {[type]}            [description]
		 */
		var mapMixins = function(mixins, controller) {
		    mixins.map(function(mixin) { inheritMixin(mixin, controller); }.bind(this));
		};

	/**
		 * [inheritMixin description]
		 * @param  {[type]} mixin   [description]
		 * @param  {[type]} controller [description]
		 * @return {[type]}            [description]
		 */
		var inheritMixin = function(mixin, controller) {
			__.inherit(mixin, controller);
		};

	if (!framework) {
		framework = {};
	}

	if (!framework.controller) {
		framework.controller = {};
	}

	framework.controller.inherit = inherit;

})(window.framework);;(function(framework) {
	/**
	 * [validateOptions description]
	 * @return {[type]} [description]
	 */
	var validate = function() {
		
		if(!__.verifyPropertiesExist(options, ['model', 'view', 'controller', 'name', 'number'])) {
		    throw new Exception('controller options are missing a required property');
		}
 		
		if(!validateMethods(options.controller.controller)) {
		    throw new Exception('Controllers must have an `init` and a `listen` method');
		}

		return true;

	};

	/**
	 * [validateMethods description]
	 * @param  {[type]} controller [description]
	 * @return {[type]}            [description]
	 */
	var validateMethods = function(controller) {
		return __.validateMethods(controller, ['init', 'listen']);
	};

	if (!framework) {
		framework = {};
	}

	if (!framework.controller) {
		framework.controller = {};
	}

	framework.controller.validate = validate;

})(window.framework);;/**
 * the domain class is an interface for providing single line
 * instantiation for a domain's model, view and controller.
 * 
 */

var Domain = function(options) {

	if (typeof options !== 'object' ||
		!options.model ||
		!options.view ||
		!options.controller) {
		return false;
	}

	this.options = __.cloneObject(options);
	this.count = 0;
	this.init(options);

};

/**
 * instantiates the view
 * this loads the template and inserts it into the dom
 * @param  {[type]} options [description]
 * @return {[type]}         [description]
 */
Domain.prototype.initView = function(options) {
	var view;

	try {
		view = framework.view.init({
			view: options.view,
			name: options.name,
			number: this.count
		});

	} catch (e) {
		throw new Error(e);
	}

	return view;

};

/**
 * [initModel description]
 * @param  {[type]} options [description]
 * @return {[type]}         [description]
 */
Domain.prototype.initModel = function(options) {
	var model;
	
	try {
		model = framework.model(options.model);
	} catch(e) {
		throw new Error(e);
	}

	return model;
};

/**
 * [initController description]
 * @param  {[type]} options [description]
 * @return {[type]}         [description]
 */
Domain.prototype.initController = function(options) {
	var controller;

	try {
		controller = framework.controller.init({
			model: options.model,
			view: options.view,
			controller: options.controller,
			name: options.name,
			number: this.count,
			mixins: options.mixins
		});

	} catch(e) {
		throw new Error(e);
	}
		
	return controller;
};

/**
 * @todo if no number property, add one
 * @param  {[type]} options [description]
 * @return {[type]}         [description]
 */
Domain.prototype.init = function(options) {
	__.runFunctions({
		fn: [
			this.initView,
			this.initModel,
			this.initController
		],
		args: [options],
		scope: this
	});

};

/**
 * @todo refactor chaining to allow for multiple arguments
 * @param  {[type]} options [description]
 * @return {[type]}         [description]
 */
Domain.prototype.updateOptions = function(options) {
	return options[1] === undefined ? options[0] : __.updateObjectProperties(options[0], options[1]);
};

Domain.prototype.updateOptionLoadOnInit = function(options) {
	return __.updateObjectProperties(options, {
		view: {
			options: {
				loadOnInit: true
			}
		}
	});
};

/**
 * [add description]
 * @param {[type]} options [description]
 */
Domain.prototype.add = function(options) {
	// @todo hack
	this.count = this.count + 1;
	return __.chainMethods({
		functions: [this.updateOptions, this.updateOptionLoadOnInit, this.init],
		args: [__.cloneObject(this.options), options],
		scope: this
	});

};

if (!framework || framework === 'undefined') {
	window.framework = {};
}

framework.Domain = Domain;;/**
 * The generic model validates each property based on type
 * finds any dom elements that are newly inserted and adds them to the model
 * and returns an the domains object model of validated properties
 */

/**
 * [Model description]
 * @param {[type]} options [description]
 */
(function(framework) {

	var model = function(options) {

		function validateModel() {

			if (!validateProperties()) {
				throw new Exception('properties are undefined');
			}

			if (!verifyRequiredOptions()) {
				throw new Exception('required model options are missing');
			}

			if (!verifyRequiredProperties()) {
				throw new Exception('required model properties are missing');
			}
			
			var modelValidation = validateSchema();

			if (modelValidation !== true) {
				modelValidation.forEach(function(error) { console.log(error.error); }.bind(this));
				throw new Exception('model schema for `'+options.properties.name+'` did not validate');
			}
			
		}

		var verifyRequiredOptions = function verifyRequiredOptions() {
			return __.verifyPropertiesExist(options, ['schema', 'properties']);
		};

		function verifyRequiredProperties() {
			return __.verifyPropertiesExist(options.properties, []);
		}

		function validateProperties() {
			return __.validateAllProperties(options);
		}

		function validateSchema() {
			return __validate(options.properties, options.schema);
		}

		// var initFunctions = [validateModel];

		// function init() {
		// 	  initFunctions.forEach(function(fn) { fn.call(this); }.bind(this));
		// }

		__.runFunctions({
			fn: [validateModel],
			scope: this,
			args: [options]
		});
 
		//init();

		return options;

	};

	framework.model = model;

})(window.framework);;/**
 * the generic view fetches the template from server or
 * accepts an html string and handles inserting the dom
 * @todo  bundle multiple template requests
 * 
 */
(function(framework, events) {

 	/**
	 * [View description]
	 * @param {[type]} options [description]
	 */
	var init = function(options) {
		/**
		 * @todo  organize constructor properties
		 * [endpoint description]
		 * @type {[type]}
		 */
		function formatOptions(options) {
			/**
			 * [addCount description]
			 * @todo  refactor
			 * @param {[type]} options [description]
			 * @param {[type]} number  [description]
			 */
			function addCount(options) {
				options[0].properties.number = options[1];
				return options;
			}

			/**
			 * [formatLoadOnInit description]
			 * @param  {[type]} options [description]
			 * @return {[type]}         [description]
			 */
			function formatLoadOnInit(options) {
				options[0].options.loadOnInit = options[0].options.loadOnInit === undefined ? true : options[0].options.loadOnInit;
				return options[0];
			}

			/**
			 * [functions description]
			 * @type {Array}
			 */
			return __.chainMethods({
				functions: [addCount, formatLoadOnInit],
				scope: this,
				args: [options.view, options.number]
			});

		}

		/**
		 * [view description]
		 * @type {[type]}
		 */
		var view = __.runFunctions({
			fn: [framework.view.validate, framework.view.listen, framework.view.sort],
			scope: this,
			args: [formatOptions(options)]
		});

	};

	/**
	 * [if description]
	 * @param  {[type]} !framework [description]
	 * @return {[type]}            [description]
	 */
	if (!framework) {
		framework = {};
	}

	/**
	 * [if description]
	 * @param  {[type]} !framework.view [description]
	 * @return {[type]}                 [description]
	 */
	if (!framework.view) {
		framework.view = {};
	}

	/**
	 * [init description]
	 * @type {[type]}
	 */
	framework.view.init = init;

})(window.framework, window.events);;(function(framework, events) {
	/**
	 * [insert description]
	 * @param  {[type]} options  [description]
	 * @param  {[type]} template [description]
	 * @return {[type]}          [description]
	 */
	function insert(options, template) {
		return __.runFunctions({
			fn: [append, emit],
			scope: this,
			args: [options.properties.name + '-' + options.properties.number, options.properties.parent, template]
		});
	}

	/**
	 * [append description]
	 * @param  {[type]} name     [description]
	 * @param  {[type]} parent   [description]
	 * @param  {[type]} template [description]
	 * @return {[type]}          [description]
	 */
	function append(name, parent, template) {
		parent.appendChild($$.stringToNode(template));
	}

	/**
	 * [emit description]
	 * @param  {[type]} name     [description]
	 * @param  {[type]} parent   [description]
	 * @param  {[type]} template [description]
	 * @return {[type]}          [description]
	 */
	function emit(name, parent, template) {
		events.emit(name + '-view-loaded');
	}

	if (!framework) {
		framework = {};
	}

	if (!framework.view) {
		framework.view = {};
	}

	framework.view.insert = insert;

})(window.framework, window.events);;(function(framework, events) {

	/**
	 * [listen description]
	 * @param  {[type]} options [description]
	 * @return {[type]}         [description]
	 */
	function listen(options) {
		events.addListener(options.properties.name + "-" + options.properties.number + "-template-transfer-complete", function(data) { framework.view.parseResults.call(this, options, data); }.bind(this));
	}

	if (!framework) {
		framework = {};
	}

	if (!framework.view) {
		framework.view = {};
	}

	framework.view.listen = listen;

})(window.framework, window.events);;(function(framework, events) {

	/**
	 * [parseResults description]
	 * @param  {[type]} options [description]
	 * @param  {[type]} data    [description]
	 * @return {[type]}         [description]
	 */
	function parseResults(options, data) {
		framework.view.insert(options, JSON.parse(data.target.response).template);
	}

	if (!framework) {
		framework = {};
	}

	if (!framework.view) {
		framework.view = {};
	}

	framework.view.parseResults = parseResults;

})(window.framework, window.events);;(function(framework, events) {

	/**
	 * [parse description]
	 * @param  {[type]} apiOptions [description]
	 * @return {[type]}            [description]
	 */
	function request(apiOptions, data) {

		/**
		 * [init description]
		 * @param  {[type]} apiOptions [description]
		 * @return {[type]}            [description]
		 */
		function init(apiOptions, data) {
			try {
				
				__.runFunctions({
					fn: [sendRequest],
					scope: this,
					args: [apiOptions, data]
				});

			} catch(e) {
				console.log(e);
				throw new Error(e.message);
			}
		
		}

		/**
		 * [formatRequest description]
		 * @param  {[type]} apiOptions [description]
		 * @return {[type]}            [description]
		 */
		function formatRequest(apiOptions) {

			__.updateObjectProperties(apiOptions, {
				data: {
					name: name
				}
			});
		
		}

		/**
		 * [sendRequest description]
		 * @param  {[type]} apiOptions [description]
		 * @return {[type]}            [description]
		 */
		function sendRequest(apiOptions, data) {
			new Request({
				name: data.name + "-" + data.number + "-template",
	            url: apiOptions.config.protocol + '://' + apiOptions.config.host + ':' + apiOptions.config.port + '/' + apiOptions.config.path,
	            authentication: apiOptions.config.authentication || null,
	            method: apiOptions.config.method || 'POST',
	            data: {
	            	name: apiOptions.template,
	            	data: data
	            },
	            callback: apiOptions.callback || null,
	            headers: apiOptions.config.headers || {}
	        });

		}

		init.call(this, __.cloneObject(apiOptions), __.cloneObject(data));
	
	}


	if (!framework) {
		framework = {};
	}

	if (!framework.view) {
		framework.view = {};
	}

	framework.view.request = request;


})(window.framework, window.events);;(function(framework, events) {

	/**
	 * [sort description]
	 * @param  {[type]} options [description]
	 * @return {[type]}         [description]
	 */
	function sort(options) {

		if (options.options.loadOnInit &&
			options.properties.template) {
			framework.view.insert(options, options.properties.template);
		}

		// @todo  validate api object for required properties
		if (options.options.loadOnInit &&
			!options.properties.template &&
			options.options.api) {
			framework.view.request(options.options.api, options.properties);
		}
				
	}

	if (!framework) {
		framework = {};
	}

	if (!framework.view) {
		framework.view = {};
	}

	framework.view.sort = sort;

})(window.framework, window.events);;(function(framework, events) {

	/**
	 * [validate description]
	 * @param  {[type]} options [description]
	 * @return {[type]}         [description]
	 */
	function validate(options) {
		/**
		 * [initializeValidation description]
		 * @param  {[type]} options [description]
		 * @return {[type]}         [description]
		 */
		function initializeValidation(options) {
			if (!validateRequiredOptions(options)) {
				throw new Exception('required options for the view are missing');
			}

			if (!validateApiOptions(options)) {
				throw new Exception('required api options for the view are missing');
			}

			var viewModelValidation = validateViewModel(options);
			
			if (viewModelValidation !== true) {
				  viewModelValidation.forEach(function(error) { console.log(error.error); }.bind(this));
				  throw new Exception('view model schema did not validate');
			}

			return true;

		}


		/**
		 * [validateRequiredOptions description]
		 * @param  {[type]} options [description]
		 * @return {[type]}         [description]
		 */
		function validateRequiredOptions(options) {
			return __.verifyPropertiesExist(options.options, []);
		}

		/**
		 * [validateApiOptions description]
		 * @param  {[type]} options [description]
		 * @return {[type]}         [description]
		 */
		function validateApiOptions(options) {
			return __.verifyPropertiesExist(options.options.api, ['template']);
		}

		/**
		 * [validateViewModel description]
		 * @param  {[type]} options [description]
		 * @return {[type]}         [description]
		 */
		function validateViewModel(options) {
		    return __validate(options.properties, options.schema);
		}

		initializeValidation.call(this, options);

	}

	if (!framework) {
		framework = {};
	}

	if (!framework.view) {
		framework.view = {};
	}

	framework.view.validate = validate;

})(window.framework, window.events);